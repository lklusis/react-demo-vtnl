import React, { Component } from 'react';
import styles from './input.less';
import { Input as AntdInput } from 'antd';

import { uniqueId } from 'lodash/fp'

// TODO: setup babel-plugin-lodash
// https://github.com/lodash/babel-plugin-lodash 
export class Input extends Component {

  componentWillMount() {
    this.id = uniqueId();
  }

  render() {
    return (
      <div className={styles.inputWrapper}>
        <AntdInput {...this.props} className={styles.input} addonBefore={this.props.label}/>
      </div>
    );

  }
}
export default Input;
