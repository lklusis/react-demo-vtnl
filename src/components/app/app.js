import React, { Component } from 'react';
import styles from './app.less';
import { Sidebar } from 'components/sidebar';
import { ContentLayout } from 'components/content-layout';

export class App extends Component {
    render() {
        return (
            <div className={styles.Homepage} >
                <div className={styles.Sidebar} >
                    <Sidebar />
                </div>
                <div className={styles.Content} >
                    <ContentLayout />
                </div>
            </div>
        );
    }
}

export default App;
