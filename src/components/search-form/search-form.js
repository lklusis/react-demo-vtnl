import React, { Component } from 'react';
import styles from './search-form.less';

import { Row, Col } from 'antd';

import { Input } from 'buildingblocks/input'
import { AutoComplete } from 'buildingblocks/autocomplete';
import { LabelledInput } from 'buildingblocks/labelledInput';
import { CountriesAutocomplete } from 'components/countriesAutocomplete';
import { Button } from 'buildingblocks/button';

export function SearchForm() {

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log('Form submitted');
    }

    return (
        <div className={styles.searchForm}>
            <form onSubmit={handleSubmit}>
                <Row>
                    <Col xs={24}>Search form</Col>
                </Row>
                <Row>
                    <CountriesAutocomplete label={'From'} placeholder={'Pick a country'} className={styles.searchFormRow} fixed={true} />
                    <CountriesAutocomplete label={'To'} placeholder={'Pick a country'} className={styles.searchFormRow} fixed={true} />
                </Row>
                <Row>
                    <Button type="submit">Search</Button>
                </Row>
            </form>
        </div>
    );
}