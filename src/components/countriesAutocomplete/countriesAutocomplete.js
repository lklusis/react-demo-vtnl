import React, { Component } from 'react';
import styles from './countriesAutocomplete.less';
import { AutoComplete } from 'buildingblocks/autocomplete';
import { LabelledInput } from 'buildingblocks/labelledInput';
import { COUNTRIES } from './countries'
import { castArray } from 'lodash/fp'
import * as reactStringReplace from 'react-string-replace';


import { AutoComplete as AntdAutoComplete } from 'antd';
const Option = AntdAutoComplete.Option;

export class CountriesAutocomplete extends Component {

    setDefaultDataSource() {
        const dataSource = COUNTRIES.map(country => {
            return (
                <Option key={country.iso2} text={country.name}>
                    {country.name}
                </Option>
            )
        });
        this.setState({ dataSource: dataSource });
    }

    componentWillMount() {
        this.setDefaultDataSource();
    }

    flattenCountryObj(country) {
        return [
            country.name,
            country.iso2,
            country.iso3,
            country.isoNo + "",
            ...country.aliases
        ];
    }

    searchCountries = (query) => {
        const queryLowCase = query.toLowerCase();

        let matchingCountries;

        if (queryLowCase.length === 1) {
            matchingCountries = COUNTRIES.filter(country => {
                return country.name[0].toLowerCase() === queryLowCase;
            });
        }
        else {
            matchingCountries = COUNTRIES.filter(country => {
                const names = this.flattenCountryObj(country);

                for (let name of names) {
                    if (name.toLowerCase().indexOf(queryLowCase) > -1) {
                        return true;
                    }
                }
                return false;
            });
        }

        return matchingCountries;
    }

    handleSearch = (queryValue) => {
        let result = this.searchCountries(queryValue).map(country => {
            return this.renderOption(country, queryValue);
        });

        this.setState({ dataSource: result });
    }

    renderOption = (country, query) => {

        let displayText = country.name;

        if (query.length > 1) {
            const queryLowCase = query.toLowerCase();

            let matchingParts = this.flattenCountryObj(country);
            // the first element is the country name, which will be always included
            matchingParts.shift();
            matchingParts = castArray(matchingParts)
                .filter(part => {
                    return part.toLowerCase().indexOf(queryLowCase) > -1
                });

            if (matchingParts.length > 0) {
                displayText += ` (${matchingParts.join(' | ')})`
            }
        }

        return (
            <Option key={country.iso2} text={country.name}>
                {reactStringReplace(displayText, new RegExp(`(${query})`, 'i'), (match, i) => (
                    <span className={styles.queryMatch}>{match}</span>
                ))}
            </Option>
        );
    }

    render() {

        const {
            className,
            label,
            placeholder,
            ...restProps,
          } = this.props;

        return (
            <div className={`${styles.wrapper} ${className}`}>
                <LabelledInput label={label} className={styles.searchFormRow} >
                    <AutoComplete
                        placeholder={placeholder}
                        dataSource={this.state.dataSource}
                        handleSearch={this.handleSearch}
                        optionLabelProp="text"
                        {...restProps}
                    />
                </LabelledInput>
            </div>
        );
    }
}

export default CountriesAutocomplete;
