import React from 'react';
import ReactDOM from 'react-dom';
import { CountriesAutocomplete } from './index';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<CountriesAutocomplete />, div);
});
