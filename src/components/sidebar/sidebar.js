import React, { Component } from 'react';
import styles from './sidebar.less';
import { SearchForm } from 'components/search-form'

export class Sidebar extends Component {
  render() {
    return (
      <div className={styles.searchFormContainer} >
        <SearchForm></SearchForm>
      </div>
    );
  }
}

export default Sidebar;
