import React, { Component } from 'react';


export class ContentLayout extends Component {
  render() {
    return (
      <div>
        <div style={{background:'#CCC', display:'block', width:'100%', height:'500px', textAlign:'center'}} >Block A</div>
        <div style={{background:'#FFF', display:'block', width:'100%', height:'500px', textAlign:'center'}} >Block B</div>
        <div style={{background:'#AFF', display:'block', width:'100%', height:'500px', textAlign:'center'}} >Block C</div>
        <div style={{background:'#FFF', display:'block', width:'100%', height:'500px', textAlign:'center'}} >Block D</div>
      </div>
    );
  }
}

export default ContentLayout;
