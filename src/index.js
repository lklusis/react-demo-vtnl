import React from 'react';
import ReactDOM from 'react-dom';
import './styles/style.less';
import App from 'components/app';
import registerServiceWorker from './registerServiceWorker';

import WebFont from 'webfontloader';

WebFont.load({
    google: {
      families: ['Open Sans:400', 'sans-serif']
      }
  });

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
